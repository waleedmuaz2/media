@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                   <form>
                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                             <a href="{{ url('/login/facebook') }}" class="btn btn-facebook"> Facebook</a>
                             
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
